# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Project 2 Requirements:

*Three Parts:*

1. Reverse-engineer lis4369_p2_requirements 
2. Produce values from lis4369_p2.R
3. Display 2 graphs using QPlot and Plot 

#### README.md file should include the following items:

* GIF of LIS4369_P2
* Link to lis4369_p2_output: [lis4369_p2_output](lis4369_p2_output.txt "P2 Output")  


#### Assignment Screenshots:

*GIF of Project 2 using MTCars Data:*

![LIS4369 P2](https://media.giphy.com/media/TPE7YuAnEDy6CSuDVs/giphy.gif)


