# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Assignment 5 Requirements:

*Four Parts:*

1. Learning to use R Tutorial
2. Code and Run demo.py
3. Skillset 13, 14, and 15
4. Screenshots of RStudio and Plots

#### README.md file should include the following items:

* Screenshots of learn_to_use_r.R Tutorial
* Screenshots of lis4369_a5 RStudio 
* Screenshots of Python Skill Sets

#### Assignment Screenshots:

*GIF of Learning to use R Tutorial:*

![Learning to use R](https://media.giphy.com/media/0B2FVd8LryR7QY8t6Q/giphy.gif)

*GIF of LIS4369 Titanic Data Set:*

![Titanic Data Set](https://media.giphy.com/media/aaA2XwG75PNSVlibfW/giphy.gif)

*GIF of Skill Sets 13-15:*

![Skill Sets 13-15](https://media.giphy.com/media/MeuKF4U0BsogtJ0dUR/giphy.gif)

