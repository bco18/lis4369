def get_requirements():
    print("Program Requirements")
    print("Developer: Brennan O'Hara\n")
    print("Python Calculator with Error Handling")
    print("1. Program calculates two numbers, and rounds to two decimal places.")
    print("2. Prompt user for two numbers, and a suitable operator.")
    print("3. Use Python error handling to validate data.")
    print("4. Test for correct arithmetic operator.")
    print("5. Division by zero not permitted.")
    print("6. Note: Program loops until correct input entered - numbers and arithmetic operator.")
    print("7. Replicate display below. \n")

def calculate_num():
    num1 = checkNum("\nEnter number 1: ")
    num2 = checkNum("\nEnter number 2: ")

    op = checkOp()

    if op == '+':
        total = num1 + num2

    elif op == '-':
        total = num1 - num2

    elif op == '*':
        total = num1 * num2

    elif op == '/':
        while True:
            try:
                total = num1 / num2
                break
            except ZeroDivisionError:
                num2 = checkNum("Cannot divide by zero! Enter num2 again: ")

    elif op == '//':
        while True:
            try:
                total = num1 // num2
                break
            except ZeroDivisionError:
                num2 = checkNum("Cannot divide by zero! Enter num2 again: ")
    elif op == '%':
        while True:
            try:
                total = num1 % num2
                break
            except ZeroDivisionError:
                num2 = checkNum("Cannot divide by zero! Enter num2 again: ")
    elif op == '**':
        total = num1 ** num2

    print("\nYour answer is: " + "{:,.2f}".format(total))

    print("\nThank you for using our Math Calculator!")

def checkNum(num):
    while True:
      try:
          return float(input(num))
      except ValueError:
         print("Not valid number!")
         continue
      else:
         break

def checkOp():
    operator = ['+', '-', '*', '/', '//', '&', '**']

    print("\nSuitable Operators: +, -, *, /, //(integer division), & (modulo operator), ** (power)")

    while True:
      op = input("Enter Operator: ")
      try:
          operator.index(op)
          return op
      except ValueError:
         print("Invaild Operators! Try again.\n")
