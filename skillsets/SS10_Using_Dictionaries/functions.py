
def get_requirements():
    print("Developer: Brennan O'Hara")
    print("Python Dictionaries")
    print("\nProgram Requirements\n"
        + "1. Dictionaries (Python data structure): unordered key:value pairs. \n"
        + "2. Dictionary: an associative array (also known as hashes).\n"
        + "3. Any key in  a dictionary is associated (or mapped) to a value (i.e., any Python data type.\n "
        + "4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unique/\n"
        + "5. Values: can be any data type and can repeat.\n"
        + "6. Create a program that mirrors the following IPO (input/process/output) format.\n"
        + "\tCreate empty dictionary, using curly braces {}: my_dictionary = {}\n"
        + "\tUse the following keys: fname, lname, degree, major, gpa\n"
        + "Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set.")


def show_dictionary():
    print("Input:\n")
    
    my_dictionary = {}

    my_dictionary = {
        "fname": input("First Name: "),
        "lname": input("Last Name: "),
        "degree": input("Degree: "),
        "major": input("Major (IT or ICT): "),
        "gpa": float(input("GPA (Better be good): "))
    }
    
    print("\nOutput:")
    print("Print my_dictionary:")
    print(my_dictionary)

    print()

    print("Return view of dictionary's (key, value) pair, built in function:")
    print(my_dictionary.items())

    print()

    print("Return view object of all keys, built-in function:")
    print(my_dictionary.keys())

    print()

    print("Return view of object of all keys, built-in function:")
    print(my_dictionary.values())

    print()

    print("Print only first and last names, using keys:")
    print(my_dictionary['fname'], my_dictionary['lname'])

    print()

    print("Print only first and last names, using get() function:")
    print(my_dictionary.get('fname'),my_dictionary.get('lname'))

    print()

    print("Count number of items (key:value pairs) in dictionary:")
    print(len(my_dictionary))

    print()

    print("Remove last directory item (popitem):")
    my_dictionary.popitem()
    print(my_dictionary)

    print()

    print("Delete major from dictionary, using key:")
    my_dictionary.pop("major")
    print(my_dictionary)

    print()

    print("Return object type: ")
    print(type(my_dictionary))

    print()

    print("Delete all items from list:")
    my_dictionary.clear()
    print(my_dictionary)