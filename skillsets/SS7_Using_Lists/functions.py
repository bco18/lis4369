
def get_requirements():
    print("Python Lists")
    print("Developer: Brennan O'Hara")
    print("\nProgram Requirements\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements \n"
        + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
        + "3. Create list - using square brackets [list]: my_list = ['cherries', 'apples', 'bananas', 'oranges'].\n"
        + "4. Create a program that mirrors the following IPO (input/process/output) format."
        + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run)")



def calculate_sqft_to_acre():

    my_list = []
    # Input
    print("\nInput:")
    number_of_elements = int(input("Enter number of list elements: "))
    print('\n')
    for i in range(0, number_of_elements):
        ele = input("Please enter list element " + str(i+1) + ': ' )
        my_list.append(ele)

    # Output
    print("\nOutput: ")
    print("Print my_list:")
    print(my_list)

    new_append = input("\nPlease enter list element: ")
    index_location = int(input("Please enter list *index* position (note: must convert to int): "))
    my_list.insert(index_location,new_append)

    print("\nInsert element into specific position in my_list")
    print(my_list)

    print("\nCount number of elements in list: ")
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort()
    print(my_list)

    print("\nReverse list:")
    my_list.sort(reverse = True)
    print(my_list)

    print("\nRemove last list element:")
    my_list.pop()
    print(my_list)

    print("\nDelete second element from list by *index* (note: 1=2nd element):")
    my_list.pop(1)
    print(my_list)

    print("\nDelete element from list by *value* (cherries):")
    my_list.remove('cherries')
    print(my_list)

    print("\nDelete all elements from list:")
    my_list.clear()
    print(my_list)