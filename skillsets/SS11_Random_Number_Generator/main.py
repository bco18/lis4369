import functions as f


def main():
    f.get_requirements()
    f.generate_random()

if __name__ == "__main__":
    main()