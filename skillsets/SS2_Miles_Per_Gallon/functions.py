#!/usr/bin/env python3
"""
Defines two functions:
1. get_requirements
2. calculate_mpg()
"""

def get_requirements():
    """Accepts 0 args. Displays program requirements."""
    print("Developer: Brennan O'Hara")
    print("Miles per Gallon")
    print("\nProgram Requirements\n"
        + "1. Convert MPG. \n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to use two decimal places.\n ")


def calc_mpg():
    print("Input:")
    miles = float(input("Enter miles driven: "))
    gals = float(input("Enter gallons of fuel used: "))
    
    mpg = miles / gals

    print("\nOutput")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f} {5}".format(miles, "miles driven and",gals,"gallons used =", mpg,"mpg"))