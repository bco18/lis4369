def get_requirements():
   print("Developer: Brennan O'Hara")
   print("Rain Detector")
   print("\nProgram Requirements:\n"
       + "1. Capture user-entered input. \n"
       + "2. Based upon day of week, determine if rain will occur.\n"
       + "3. Notel: Program does case-insensitive comparison.\n")


def get_weather():
    print("Input: ")
    day = str(input("Enter full name of day of week: ")).lower()

    print("\nOutput: ")

    if (day == 'saturday'):
        print("It's the weekend")
    elif (day == 'sunday'):
        print("It's the weekend")
    else:
        print("Better get an umbrella!")


