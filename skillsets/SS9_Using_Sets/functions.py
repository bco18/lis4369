
def get_requirements():
    print("Python Sets - like mathematical sets!")
    print("Developer: Brennan O'Hara")
    print("\nProgram Requirements\n"
        + "1. Sets (Python date structure): mutable, heterogeneous, unordered sequence of elements, *but* cannot hold duplicate values. \n"
        + "2. Sets are mutable/changeable--that is, can insert, update, delete. \n"
        + "3. While sets are mutable/changeable, they *cannot* contain other mutable items like list, set, or dictionary--.\n"
        + "\tthat is, elements contained in set must be immutable.\n"
        + "4. Also, since sets are unordered, cannot use indexing (or, slicing) to access, update, or delete elements.\n"
        + "5. Two methods to create sets:\n"
        + "\ta. Create set using curly brackets {set}:my_set {1,3.14,2.0,'four','Five'}\n"
        + "\tb. Create set using set() function: my_set = set(<iterable>)\n"
        + "\tExamples:\n"
        + "\tmy_set1 = set([1, 3.14, 2.0, 'four', 'Five']) # set with list\n"
        + "\tmy_set2 = set((1, 3.14, 2.0, 'four', 'Five')) # set with tuple\n"
        + "Note: An 'iterable' is *any* object, which can be iterated over--that is, lists, tuples, or even strings.\n"
        + "6. Create a program that mirrors the following IPO (input/process/output) format.\n")


def print_sets():
    print("Input: Hard coded--no user input. See three examples above.")
    print("***Note***: All threee sets below print as 'sets' (i.e., curly brackets), *regardless* of how they were created!")
    
    print()

    
    print("Print my_set crated using curly brackets:")
    my_set = {1,3.14,2.0,'four','Five'}
    print(my_set)

    print()

    print("Print type of my_set: ")
    print(type(my_set))

    print()

    print("Print my_set1 created using set() function with list:")
    my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
    print(my_set1)

    print()

    print("Print type of my_set1:")
    print(type(my_set1))

    print()

    print("Print my_set2 created using set() function with tuple:")
    my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))
    print(my_set2)

    print()

    print("Print type of my_set2:")
    print(type(my_set2))

    print()

    print("Length of my_set:")
    print(len(my_set))

    print()

    print("Discard 'four':")
    my_set.discard('four')
    print(my_set)

    print()

    print("Remove 'Five':")
    my_set.remove('Five')
    print(my_set)

    print()

    print("Length of my_set:")
    print(len(my_set))

    print()

    print("Add element to set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print()

    print("Length of my_set:")
    print(len(my_set))

    print()

    print("Display minimum number:")
    print(min(my_set))

    print()

    print("Display maximum number:")
    print(max(my_set))

    print()

    print("Display sum of numbers:")
    print(sum(my_set))

    print()

    print("Delete all set elements:")
    my_set.clear()
    print(my_set)

    print()

    print("Length of my_set:")
    print(len(my_set))




    