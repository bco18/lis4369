import functions as f


def main():
    f.get_requirements()
    f.print_tuples()

if __name__ == "__main__":
    main()