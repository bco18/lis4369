import functions as f


def main():
    f.get_requirements()
    f.read_file()

if __name__ == "__main__":
    main()