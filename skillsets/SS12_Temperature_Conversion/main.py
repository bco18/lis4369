import functions as f


def main():
    f.get_requirements()
    f.convert_temperature()

if __name__ == "__main__":
    main()