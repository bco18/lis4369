CAL_PER_FAT = 9
CAL_PER_CARB = 4
CAL_PER_PROTEIN = 4

def get_requirements():
   print("Developer: Brennan O'Hara")
   print("Calorie Percentages")
   print("\nProgram Requirements:\n"
       + "1. Find calories per grams of fat, carbs, and protein. \n"
       + "2. Calculate percentages.\n"
       + "3. Must use float data types.\n"
       + "4. Format, right-align numbers, and round to two decimal places.\n")


def calculate_percentages():
   print("Input:")
   total_fat = float(input("Enter total fat grams: "))
   total_carb = float(input("Enter total carb grams: "))
   total_protein = float(input("Enter total protein grams: "))

   fat_calories = total_fat * CAL_PER_FAT
   carb_calories = total_carb * CAL_PER_CARB
   protein_calories = total_protein * CAL_PER_PROTEIN

   total_calories = fat_calories + carb_calories + protein_calories
   fat_percent = (fat_calories / total_calories) * 100
   carb_percent = (carb_calories / total_calories) * 100
   protein_percent = (protein_calories / total_calories) * 100

   print("\nOutput:")
   print('{0:<8} {1:<10} {2:>12}'.format('Type', 'Calories', 'Percentage'))
   print('{0:<8} {1:<10,.2f} {2:>10.2f} {3}'.format("Fat", fat_calories, fat_percent, '%'))
   print('{0:<8} {1:<10,.2f} {2:>10.2f} {3}'.format("Carbs", carb_calories, carb_percent, '%'))
   print('{0:<8} {1:<10,.2f} {2:>10.2f} {3}'.format("Protein", protein_calories, protein_percent, '%'))


  # docs.python.org/3/library/string.html#format-specification-mini-language

  # docs.python.org/3/library/string.html#string-formatting

  # python-course.eu/python3_formatted_output.php

  # digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3
