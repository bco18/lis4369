import functions as f


def main():
    f.get_requirements()
    f.show_loops()

if __name__ == "__main__":
    main()
