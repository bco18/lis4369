"""
Defines two functions:
1. get_requirements
2. calculate_percent)
"""

def get_requirements():
    """Accepts 0 args. Displays program requirements."""
    print("Developer: Brennan O'Hara")
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements\n"
        + "1. Find number of IT/ICT students in class. \n"
        + "2. Calculate IT/ICT Student Percentage.\n"
        + "3. Format, right-align numbers, and round to two decimal places.\n ")


def calculate_percent():
    print("Input:")
    it_students = float(input("Enter number of IT students: "))
    ict_students = float(input("Enter number of ICT students: "))
    
    total_students = (it_students + ict_students)
    it_percent = (it_students / total_students) * 100
    ict_percent = (ict_students / total_students) * 100
    
    print("\nOutput:")
    print(f'Total Students: {total_students:>8.2f}')
    print(f'IT Students: {it_percent:10.2f}%')
    print(f'ICT Students: {ict_percent:>9.2f}%\n')
