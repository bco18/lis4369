import functions as f


def main():
    f.get_requirements()
    f.calculate_payroll()


# should be (missing double underscores at end): if __name__ == "__main__":
if __name__ == "__main__":
    main()
