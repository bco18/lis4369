> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Assignment 2 Requirements:

*Three Parts:*

1. Payroll Application
2. Chapter Questions (Chs 3,4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running payroll application
* Link to A2 .ipynb file: [a2_payroll.ipynb](a2_payroll/a2_payroll.ipynb "A2 Jupyter Notebook")
* Screenshots of Python Skill Sets

#### Assignment Screenshots:

*Screenshots of Payroll Application:*

| Payroll No Overtime                                          | Payroll with Overtime                                          |
|--------------------------------------------------------------|----------------------------------------------------------------|
| ![Payroll App Screenshot No Overtime](img/a2_No_Overtime.PNG)| ![Payroll App Screenshot w/ Overtime](img/a2_w_Overtime.PNG)   |

*Screenshots of Python Skill Sets:*

| Skill Set 1: Square Feet to Acres | Skill Set 2: Miles per Gallon  | Skill Set 3: IT/ICT Student Percentage|
|-----------------------------------|--------------------------------|---------------------------------------|
| ![SS1 Screenshot](img/SS1.PNG)    | ![SS2 Screenshot](img/SS2.PNG) | ![SS3 Screenshot](img/SS3.PNG)        |

*Screenshots of A2 Jupyter Notebook:*

![a2 Jupyter Notebook Pic 1](img/a2_jupyter_notebook1.PNG "A2 Jupyter Notebook Pic 1")

![a2 Jupyter Notebook Pic 2](img/a2_jupyter_notebook2.PNG "A2 Jupyter Notebook Pic 2")

![a2 Jupyter Notebook Pic 3](img/a2_jupyter_notebook3.PNG "A2 Jupyter Notebook Pic 3")

