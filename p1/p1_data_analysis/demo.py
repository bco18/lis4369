#Pandas "Python Data Analysis Library"
import datetime
import pandas_datareader as pdr #remote data acess for pandas
import matplotlib.pyplot as plt
from matplotlib import style


def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)
    # for "end": "must" use Python function for current day/time

    # Read data into Pandas DateFrame
    # NOTE: XOM is stock market symbol for Exxon Mobil Corporation
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    df.shape[0]
    # Why is it important to run the following print statement...
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)  # Note: for efficiency, only prints 60--not *all* records

    print("\nPrint first five lines:")
    df1 = df.head(3)
    print(df1)

    print("\nPrint last five lines:")
    df3 = df.tail(3)
    print(df3)

    print("\nPrint first 2 lines:")
    # statement goes here...

    print("\nPrint last 2 lines:")
    # statement goes here...

    # Research what these styles do!
    # style.use('fivethirtyeight')
    # compare with...
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()
