#Pandas "Python Data Analysis Library"
import datetime
import pandas_datareader as pdr #remote data acess for pandas
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print("\nData Analysis 1")
    print("Developer: Brennan O'Hara\n")
    print("Program Requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If erroes, more than likely missing installations.\n"
        + "3. Test Python Package Installer: pip freeze.\n"
        + "4. Research how to do the following installations.\n"
        + "\ta. pandas (only if missing)\n"
        + "\tb. pandas-datareader (only if missing)\n"
        + "\tc. matplotlib (only if missing)\n"
        + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. data_analysis_1(): displays the following data\n")



def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 23)
    # for "end": "must" use Python function for current day/time

    # Read data into Pandas DateFrame
    # NOTE: XOM is stock market symbol for Exxon Mobil Corporation
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print()

    print("Print Columns:")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df) 

    print("\nPrint first five lines:")
    df1 = df.head(5)
    print(df1)

    print("\nPrint last five lines:")
    df2 = df.tail(5)
    print(df2)

    print("\nPrint first 2 lines:")
    df3 = df.head(2)
    print(df3)

    print("\nPrint last 2 lines:")
    df4 = df.tail(2)
    print(df4)

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()

