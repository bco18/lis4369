# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Project 1 Requirements:

*Four Parts:*

1. Data Analysis 1
2. Chapter Questions
3. Skillset 7, 8, and 9
4. Bitbucket Repo Link

#### README.md file should include the following items:

* Screenshots of Data Analyis 1
* Link to P1 .ipynb file: [p1_data_analysis.ipynb](p1_data_analysis/p1_data_analysis.ipynb "P1 Jupyter Notebook")
* Screenshots of Python Skill Sets

#### Assignment Screenshots:

*GIF of Data Analysis 1:*

![Data Analysis 1 GIF](https://media.giphy.com/media/icsATc7ctPlh1FoNTv/giphy.gif)

*GIF of Jupyter Notebook:*

![Data Analysis 1 Jupyter](https://media.giphy.com/media/MlixcUiSIxPohUBoHo/giphy.gif)

*GIF of Skill Sets 7-9:*

![Skill Sets 7-9](https://media.giphy.com/media/1diitWsQcFLCas6sw4/giphy.gif)

*Screenshot of Extra Credit Skill Set:*

![Rain Detector SS](screenshots/extra.png)