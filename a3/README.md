
# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Assignment 3 Requirements:

*Three Parts:*

1. Painting Application
2. Chapter Questions
3. Skillset 4, 5, and 6
4. Bitbucket Repo Link

#### README.md file should include the following items:

* Screenshots of paint calculator application
* Link to A3 .ipynb file: [a3_painting_calc.ipynb](a3_painting_calc/a3_painting_calc.ipynb "A3 Jupyter Notebook")
* Screenshots of Python Skill Sets

#### Assignment Screenshots:

*GIF of Painting Application:*

![Painting Calculator GIF](https://media.giphy.com/media/VkIlwlLNFxTGFeg3T9/giphy.gif)

*GIF of Skill Sets 4 - 6:*

![Skill Sets 4-6](https://media.giphy.com/media/zYSn83r0MUPKTbtDOP/giphy.gif)

