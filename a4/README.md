
# LIS4369 - Extensible Enterprise Solutions (Python & R)

## Brennan O'Hara

### Assignment 4 Requirements:


1. Data Analysis 2
2. Chapter Questions
3. Skillset 10, 11, 12
4. Jupyter Notebook link

#### README.md file should include the following items:

* Screenshot of Data Analysis 2
* Link to a4 .ipynb file: [a4_data_analysis.ipynb](a4_data_analysis2/a4_data_analysis2.ipynb "A4 Jupyter Notebook")
* Screenshot of A4 Skill Sets


#### Assignment Screenshots:

*GIF of Data Analysis 2*:

![A4 Data Analysis 2](https://media.giphy.com/media/YGf2rJpLUAo8VIod2U/giphy.gif)

(The graph is split due to a lack of information(Age) for those passengers)

*Jupyter Notebook*:

![A4 Jupyter Notebook](https://media.giphy.com/media/DCfOaCgAtwtQcTeYTv/giphy-downsized-large.gif)

*GIF of Skillsets 10, 11, and 12*:

![Skill Sets 10,11,12](https://media.giphy.com/media/VJRn4uA49z5OIspsJr/giphy.gif)
